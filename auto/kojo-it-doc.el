(TeX-add-style-hook
 "kojo-it-doc"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "italian") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "babel"
    "fontenc"
    "fontspec"
    "graphicx"
    "mathtools"
    "algpseudocode"
    "algorithm"
    "listings"
    "minted"
    "hyperref"
    "ulem"
    "tikz"
    "lastpage"
    "fancyhdr"
    "geometry")
   (TeX-add-symbols
    "dontdofcolorbox")
   (LaTeX-add-labels
    "sec:orgc6e0b6a"
    "sec:orgad54a66"
    "sec:org42e2566"
    "sec:orgb8da20f"
    "sec:orgf85cfbc"
    "sec:org25b19d6"
    "sec:org33c043d"
    "sec:org388ddad"
    "sec:org44f9346"
    "sec:org52d9a50"
    "sec:org88515a9"
    "sec:org9bfb493"
    "sec:org3cdf98d"
    "sec:orgdb37f5b"
    "sec:orgfb2f67c"
    "sec:org7b2b9b4"
    "sec:org58e981b"
    "sec:org75380c0"))
 :latex)

