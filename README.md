Comandi di Kojo in italiano
==============================

Kojo è sviluppato da Lalit Pant (Himjyoti school, Dehradun - India) ed è utilizzato in varie scuole indiane, statunitensi, inglesi e svedesi.  
L'approccio usato nella piattaforma Kojo (http://www.kogics.net/kojo) è più ampio dei soliti ambienti per l'insegnamento. Può essere rivolto a più livelli di apprendimento ed è dotato di parti specifiche, per esempio per la sperimentazione in ambito matematico con un laboratorio basato su GeoGebra (http://www.geogebra.org/cms/it/). Il linguaggio utilizzato è Scala (http://www.scala-lang.org/).  
Scala è un linguaggio estremamente potente e multiparadigma (Orientato agli oggetti, funzionale) che può essere utilizzato a vari livelli, sufficientemente semplice, nelle sue basi, da poter essere insegnato in età scolare (dalla classe 4° primaria). La sua caratteristica di linguaggio funzionale lo fa particolarmente utile nella risoluzione di problemi matematici.  


